package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Collectable;

@Database(entities = {Collectable.class}, version = 8, exportSchema = false)
public abstract class CollectableRoomDatabase extends RoomDatabase {

    private static final String TAG = CollectableRoomDatabase.class.getSimpleName();

    public abstract CollectableDao collectableDao();

    private static CollectableRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CollectableRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CollectableRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CollectableRoomDatabase.class,"museum_collectable_database")
                                    .fallbackToDestructiveMigration()
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        CollectableDao dao = INSTANCE.collectableDao();

                    });

                }
            };



}
