package fr.uavignon.ceri.tp3.data.webservice;

public class Forecast {

    public final String name;
    public final String description;
    public final String brand;
    public final boolean working;

    public Forecast(String name, String description, String brand, boolean working){
        this.name = name;
        this.description = description;
        this.brand = brand;
        this. working = working;
    }

}
