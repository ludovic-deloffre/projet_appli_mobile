package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Collectable;

@Dao
public interface CollectableDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Collectable collectable);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Collectable collectable);

    @Query("DELETE FROM Collectable")
    void deleteAll();

    @Query("DELETE FROM Collectable WHERE _id = :id")
    void deleteCollectable(String id);

    @Query("SELECT * from Collectable ORDER BY name ASC")
    List<Collectable> getSynchrAllCollectables();

    @Query("SELECT * from Collectable WHERE name LIKE '%' || :req || '%'")
    List<Collectable> getCollectableBySearch(String req);

    @Query("SELECT * from Collectable ORDER BY year ASC")
    List<Collectable> getCollectablesOrderByDate();

    @Query("SELECT * FROM Collectable WHERE _id = :id")
    Collectable getCollectableById(String id);


}
