package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface CeriMuseumInterface {
    @Headers({
            "Accept: json",
    })
    @GET("collection")
    public Call<Map<String, CollectableResponse>> getCollection();
}
