package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textName, textDescription, textYear, textBrand, textState, textCategories;
    private ImageView imgCollectable;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // recup le collectable selectionné
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        String collectable_id = args.getItemNum();

        viewModel.setCollectable(collectable_id);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textName = getView().findViewById(R.id.name);
        textDescription = getView().findViewById(R.id.description);
        textYear = getView().findViewById(R.id.year);
        textBrand = getView().findViewById(R.id.brand);
        textState = getView().findViewById(R.id.state);
        textCategories = getView().findViewById(R.id.categories);
        imgCollectable = getView().findViewById(R.id.item_image);

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getCollectable().observe(getViewLifecycleOwner(),
                collectable -> {

                    if (collectable != null) {

                        textName.setText(collectable.getName());

                        if (collectable.getDescription() != null) {
                            textDescription.setText(collectable.getDescription());
                        } else {
                            textDescription.setText("- - -");
                        }

                        if (collectable.getBrand() != null) {
                            textBrand.setText(collectable.getBrand());
                        } else {
                            textBrand.setText("- - -");
                        }


                        if (collectable.getYear() == 0) {
                            textYear.setText("- - -");
                        } else {
                            textYear.setText(String.valueOf(collectable.getYear()));
                        }

                        if (collectable.getWorking() == true) {
                            textState.setText("Fonctionnel");
                        } else {
                            textState.setText("Non Fonctionnel");
                        }

                        if (collectable.getPicture() != ""){
                            Glide.with(this)
                                    .load(collectable.getPicture())
                                    .into(imgCollectable);
                        }

                        if(collectable.getCategories() != null){
                            textCategories.setText(collectable.getCategories());
                        }
                        else{
                            textCategories.setText("- - -");
                        }


                    }

                });

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        if(progress != null) {
                            progress.setVisibility(View.VISIBLE);
                        }
                    }
                    else{
                        if(progress != null){
                            progress.setVisibility(View.GONE);
                        }
                    }
                }
        );
        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        );
    }


}
