package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CollectableDao;
import fr.uavignon.ceri.tp3.data.database.CollectableRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.CollectableResponse;
import fr.uavignon.ceri.tp3.data.webservice.CeriMuseumInterface;
import fr.uavignon.ceri.tp3.data.webservice.CollectableResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.CollectableRoomDatabase.databaseWriteExecutor;

public class CeriMuseumRepository {

    private static final String TAG = CeriMuseumRepository.class.getSimpleName();

    private MutableLiveData<Collectable> selectedCollectable =new MutableLiveData<>();;

    private final CeriMuseumInterface api;

    public MutableLiveData<Boolean> isLoading=new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();

    private CollectableDao collectableDao;

    private MutableLiveData<ArrayList<Collectable>> collectableList = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Collectable>> getCollectableList() {
        return collectableList;
    }

    private static volatile CeriMuseumRepository INSTANCE;

    public synchronized static CeriMuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CeriMuseumRepository(application);
        }
        return INSTANCE;
    }

    public CeriMuseumRepository(Application application) {
        CollectableRoomDatabase db = CollectableRoomDatabase.getDatabase(application);
        collectableDao = db.collectableDao();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CeriMuseumInterface.class);
    }

    public void getCollectable(String id)  {

        Future<Collectable> collectable = databaseWriteExecutor.submit(() -> {
            return collectableDao.getCollectableById(id);
        });
        try {
            selectedCollectable.setValue(collectable.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void getCollectablesOrderByName(){
        ArrayList<Collectable> allCollectables = (ArrayList<Collectable>) collectableDao.getSynchrAllCollectables();
        collectableList.postValue(allCollectables);
    }

    public MutableLiveData<Collectable> getSelectedCollectable() {
        return selectedCollectable;
    }

    public void loadCollectionFromDatabase(){
        ArrayList<Collectable> allCollectables = (ArrayList<Collectable>) collectableDao.getSynchrAllCollectables();
        collectableList.postValue(allCollectables);
    }

    public void getCollectablesOrderByDate(){
        ArrayList<Collectable> allCollectables = (ArrayList<Collectable>) collectableDao.getCollectablesOrderByDate();
        collectableList.postValue(allCollectables);
    }

    public void getCollectableBySearch(String req){
        ArrayList<Collectable> allCollectables = (ArrayList<Collectable>) collectableDao.getCollectableBySearch(req);
        collectableList.postValue(allCollectables);
    }




    public void loadCollection(){
        isLoading.postValue(Boolean.TRUE);
        api.getCollection().enqueue(
                new Callback<Map<String, CollectableResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, CollectableResponse>> call,
                                           Response<Map<String, CollectableResponse>> response) {
                        Log.d("onResponse : ",response.body().toString());
                        ArrayList<Collectable> collectableList = new ArrayList<>();
                        int i=0;
                        for (String key: response.body().keySet()) {
//                            System.out.println(key + "=" + response.body().get(key).name);
                            CollectableResult.transferInfo(response.body().get(key), key, collectableList);
                            long res = insertCollectable(collectableList.get(i));
//                            System.out.println("INSERTION:"+res);
                            i++;

                        }

                        CeriMuseumRepository.this.collectableList.setValue(collectableList);
                        isLoading.postValue(Boolean.FALSE);

                    }

                    @Override
                    public void onFailure(Call<Map<String, CollectableResponse>> call, Throwable t) {
                        Log.d("onFailure : ",t.getMessage());
                        webServiceThrowable.postValue(t);
                    }
                });

    }

    public void removeAll(){
        collectableDao.deleteAll();
        collectableList.postValue(null);
    }


    public long insertCollectable(Collectable newCollectable) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return collectableDao.insert(newCollectable);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public void deleteCollectable(String id) {
        databaseWriteExecutor.execute(() -> {
            Log.d(TAG,"selected collectable id for delete = " + id);
            collectableDao.deleteCollectable(id);
        });
    }

}
