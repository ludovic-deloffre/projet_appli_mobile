package fr.uavignon.ceri.tp3.data.webservice;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Collectable;

public class CollectableResult {

    public static void transferInfo(CollectableResponse body, String key, ArrayList<Collectable> collectables) {

        Collectable collectable = new Collectable();
        collectable.setId(key);

        collectable.setName(body.name);
        collectable.setDescription(body.description);
        collectable.setWorking(body.working);

        if (body.year!=null){
            collectable.setYear(body.year);
        }
        else{
            collectable.setYear(0);
        }

        if (body.brand!=null){
            collectable.setBrand(body.brand);
        }
        else{
            collectable.setBrand("- - -");
        }

        if(body.pictures != null) {
            int i = 0;
            for (String img : body.pictures.keySet()) {
                if (i == 0){
                    collectable.setPicture("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+key+"/images/"+img);
                }
                i+=1;
//                System.out.println(collectable.getPicture());
            }
        }

        if(body.categories != null){
            String categorie = "";
            for(int i = 0; i< body.categories.size(); i++){
                categorie = categorie + ", " + body.categories.get(i);
            }
            String categories = categorie.substring(2);
            //System.out.println(categories);
            collectable.setCategories(categories);
        }

        collectables.add(collectable);

    }

}
