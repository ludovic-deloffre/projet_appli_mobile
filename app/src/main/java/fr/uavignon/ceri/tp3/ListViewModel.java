package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.CeriMuseumRepository;
import fr.uavignon.ceri.tp3.data.Collectable;

public class ListViewModel extends AndroidViewModel {
    private CeriMuseumRepository repository;
    private MutableLiveData<Boolean> isLoading;

    private MutableLiveData<ArrayList<Collectable>> allCollectables;

    public ListViewModel (Application application) {
        super(application);
        ArrayList<Collectable> collectables =new ArrayList<Collectable>();
        allCollectables = new MutableLiveData<>();
        repository = CeriMuseumRepository.get(application);
        isLoading = repository.isLoading;
        allCollectables = repository.getCollectableList();
    }

    // a changer : delete item de la collection
    public void deleteCollectable(String id) {
        System.out.println("Test delete");
        repository.deleteCollectable(id);
    }

    // savoir si la collection is loading
    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Collectable>> getAllCollectables() {
        return allCollectables;
    }

    public void loadCollection(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollection();
            }
        };
        t.start();
    }

    public void loadCollectionFromDatabase(){
        //repository.loadWeatherAllCities();
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDatabase();
            }
        };
        t.start();
    }

    public void removeAll(){
        Thread t = new Thread(){
            public void run(){
                repository.removeAll();
            }
        };
        t.start();
    }

    public void getCollectablesOrderByName(){
        Thread t = new Thread(){
            public void run(){
                repository.getCollectablesOrderByName();
            }
        };
        t.start();
    }

    public void getCollectablesOrderByDate(){
        Thread t = new Thread(){
            public void run(){
                repository.getCollectablesOrderByDate();
            }
        };
        t.start();
    }

    public void getCollectableBySearch(String req){
        Thread t = new Thread(){
            public void run(){
                repository.getCollectableBySearch(req);
            }
        };
        t.start();
    }



}
