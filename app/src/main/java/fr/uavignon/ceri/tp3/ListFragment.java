package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ProgressBar progress;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        listenerSetup();
        observerSetup();

    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);
        progress = getView().findViewById(R.id.progressList);
    }

    private void observerSetup() {

        viewModel.getAllCollectables().observe(getViewLifecycleOwner(),
                collectables -> adapter.setCollectableList(collectables));

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        if(progress != null){
                            progress.setVisibility(View.VISIBLE);
                        }
                    }
                    else{
                        if(progress != null) {
                            progress.setVisibility(View.GONE);
                        }
                    }
                }
        );
    }

}
