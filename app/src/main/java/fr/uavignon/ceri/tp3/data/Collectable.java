package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity(tableName = "Collectable", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Collectable {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="brand")
    private String brand="Inconnu";

    @NonNull
    @ColumnInfo(name="year")
    private int year = 0;

    @NonNull
    @ColumnInfo(name="description")
    private String description = null;

    @NonNull
    @ColumnInfo(name="working")
    private boolean working = false;

    @NonNull
    @ColumnInfo(name="picture")
    private String picture = "";

    @NonNull
    @ColumnInfo(name="categories")
    private String categories;

    public Collectable() {
    }

    public Collectable(String id, String name, String description) {
        this.id=id;
        this.name=name;
        this.description=description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand=brand;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPicture(@NonNull String picture) {
        this.picture = picture;
    }

    public void setCategories(@NonNull String categories) {
        this.categories = categories;
    }

    // getter

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public int getYear() {
        return year;
    }

    public boolean getWorking() {
        return working;
    }

    public String getDescription() {
        return description;
    }

    @NonNull
    public String getPicture() {
        return picture;
    }

    @NonNull
    public String getCategories() {
        return categories;
    }

}
