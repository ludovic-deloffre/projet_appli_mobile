package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    ListViewModel listViewModel;
    EditText searchBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listViewModel.loadCollectionFromDatabase();
        searchBar = (EditText) findViewById(R.id.inputItem);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void getTextFromSearchBar (View view){
        String requete = ((EditText)findViewById(R.id.inputItem)).getText().toString();
        listViewModel.getCollectableBySearch(requete);
        System.out.println("-------------------------- La requete : " + requete);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.update_collection) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Chargement de la collection ...",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.loadCollection();

            return true;
        }
        if (id == R.id.remove_collection) {

            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Collection supprimée !",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            listViewModel.removeAll();

            return true;
        }
        if (id == R.id.tri_annee) {

            listViewModel.getCollectablesOrderByDate();

            return true;
        }
        if (id == R.id.tri_alphabet) {

            listViewModel.getCollectablesOrderByName();

            return true;
        }
//        if (id == R.id.searchCollection) {
//
//
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
}
