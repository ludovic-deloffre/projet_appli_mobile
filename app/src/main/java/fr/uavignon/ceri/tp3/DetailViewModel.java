package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.CeriMuseumRepository;
import fr.uavignon.ceri.tp3.data.Collectable;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();
    private MutableLiveData<Boolean> isLoading;
    private CeriMuseumRepository repository;
    private MutableLiveData<Collectable> collectable;
    private  MutableLiveData<Throwable> webServiceThrowable;


    public DetailViewModel (Application application) {
        super(application);
        isLoading=new MutableLiveData<Boolean>();
        repository = CeriMuseumRepository.get(application);
        isLoading=repository.isLoading;
        webServiceThrowable=repository.webServiceThrowable;
        collectable = new MutableLiveData<>();
    }

    public void setCollectable(String id) {
        repository.getCollectable(id);
        collectable = repository.getSelectedCollectable();
    }
    LiveData<Collectable> getCollectable() {
        return collectable;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return webServiceThrowable;}


}

